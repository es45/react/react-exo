import React, {Fragment, StrictMode} from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./sources/components/app";

const rootElement = document.getElementById("root");
const root = createRoot(rootElement);

const recipe = {
  title: "Les crêpes au citron",
  ingredients: [
    "Farine",
    "Lait",
    "Sucre",
    "Sel",
    "Citron",
    "Vanille"
  ]
};

const Main = () => (
  <StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </StrictMode>
);

root.render(
  <Fragment>
    <h1>{recipe.title}</h1>
    <ul>
      {recipe.ingredients.map((ingredient, key) => (
        <li key={key}>
          {ingredient}
        </li>
      ))}
    </ul>
    <form onSubmit={event => event.preventDefault()}>
      <label htmlFor="email">Email</label>
      <input id="email" type="email" required autoFocus />
      <button type="submit">Souscrire</button>
    </form>
  </Fragment>
);
