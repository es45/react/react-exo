import React from "react";

const App = () => (
  <>
    <header>
      <title>test</title>
    </header>
    <main>
      <h1>Recette de crêpes au citron</h1>
      <ul>
        <li>Farine</li>
        <li>Lait</li>
        <li>Sucre</li>
        <li>Sel</li>
        <li>Citron</li>
        <li>Vanille</li>
      </ul>
    </main>
  </>
);

export default App;
